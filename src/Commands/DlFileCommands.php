<?php

namespace Drupal\dl_file\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class DlFileCommands extends DrushCommands {
     /**
     * @param String $dt1
     *   Argument with message to be displayed d1.

     * @command dl_file:dl-file
     * @aliases drush-dl_file ,dl-file,

     * @option date
     *   date from to date to.

     * @option info
     *   about dl_file module.

     * @usage  dl_file:dl-file --date Y/m/d,dl-file --v-help
     */
    public function dlFile ($dt1 = 'Command Not Executed. run dl-file --info to find more.', $options = ['date' => false,'info' =>false] ) {
        if($options['date']) {
            $vlid_date = date('Y/m/d', strtotime($dt1));
            $hrs = strtotime($dt1."+1 day");
            if($dt1 == $vlid_date) {
                $this->output()->writeln("<bg=cyan;options=bold;fg=white>Date formate is valid - ".$vlid_date."</>");
                $t1 = '12:00:00';
                $dt_s = $dt1.' '.$t1;
                $start_time = date('Y-m-d h:i:s', strtotime($dt_s));
                $st_time = strtotime($start_time);
                // load file based on created date.
                $database = \Drupal::database();
                $query = $database->select('file_managed', 'fm')
                    ->fields('fm', ['fid'])
                    ->fields('fm', ['uri'])
                    ->condition('fm.status', 1)
                    ->condition('fm.created', $hrs, '<=');
                // fatch file id.
                $resultEvent['Active'] = $query->execute()->fetchAll();
                $this->output()->writeln(
                    "<options=bold;fg=green>[ Total Files Found ] = [ ".count($resultEvent['Active'])." ]</>"
                );
                if ($resultEvent['Active'] != null) {
                    $this->output()->writeln(
                        "<info>\nFiles will be permanently delete from date</info>
                        <bg=black;options=bold;>\nFrom : ".date('Y-m-d h:i:s', $st_time)."\nTo : ".date('Y-m-d h:i:s', $hrs)."</>"
                    );
                    $fids[] = array();
                    $i=0;
                    if ($this->io()->confirm('<question>WARNING! Are you sure you want to delete these files?</question>')) {
                        $this->output()->writeln("<bg=cyan;>Processing Deletion</>");
                    } else {
                        throw new UserAbortException();
                    }
                    foreach ($resultEvent['Active'] as $value){
                        $fids[$i] = $value->fid;
                        $path = $value->uri;
                        $file = basename($path);
                        $this->output()->writeln($file);
                        $mnth = date("m");
                        $yr = date("Y");
                        $large = "public://styles/large/public/".$yr."-".$mnth."/".$file;
                        $thumbnail = "public://styles/thumbnail/public/".$yr."-".$mnth."/".$file;
                        $this->output()->writeln("<info> > Clening from Large  = [ ".$i." ] File Path [ ".$large." ]</info>");
                        unlink($large);
                        $this->output()->writeln("<info> > Clening from Thumbnail  = [ ".$i." ] File Path [ ".$thumbnail." ]</info>");
                        unlink($thumbnail);
                        $this->output()->writeln("<info> > Clening from ".$yr."  = [ ".$i." ] File Path [ ".$path." ]</info>");
                        unlink($path);
                        $this->output()->writeln("<bg=cyan;options=bold;>Cleaned = [ ".$i." ]</>");
                        $i++;
                    }
                    $this->output()->writeln("<info>[ Total Files are Deleted from directory ] = [ ".$i." ]</info>");
                    $query = $database->delete('file_managed');
                    $query->condition('created', $hrs, '<=');
                    $resultEvent['Active'] = $query->execute();

                    for($i = 0;$i<count($fids);$i++){
                        $query = $database->delete('file_usage');
                        $query->condition('fid', $fids[$i], '=');
                        $resultEvent['Deactive'] = $query->execute();
                    }
                } else {
                    $this->output()->writeln("<options=bold;fg=white;bg=black>[ No matching images found. Use another date ]</>");
                }
            } else {
                $this->output()->writeln("<bg=red;options=bold;fg=white>Date Foramte is Wrong!</>");
                $this->output()->writeln("<options=bold;fg=white>run below commands \ndl-file --info</>");
            }
        } elseif ($options['info']) {
            $this->output()->writeln("<bg=yellow;fg=white;options=bold>This module is used to delete files from directory and database.</><options=bold;fg=blue>\nRun Below Command</><options=bold> \n drush dl-file --date <option> as <Y/m/d> to delete files from given date +1 day.\n drush dl-file --info to know about commands.</><options=bold;fg=blue>\nThank you.</>");
        } else {
            $this->output()->writeln("<bg=red;options=bold;fg=white>".$dt1."</>");
        }
    }
} 
