Delete File
Delete File module is an simple file delete module based on 
php and drupal functionality.
this module will delete files from a given date to +1 day.

Features -

this module will help developers to delete files permanently from 
database and directory on single drush command.

Commands -

drush dl-file --date
drush dl-file --info
